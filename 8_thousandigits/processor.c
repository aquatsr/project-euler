#include <stdio.h>
#include <stdlib.h>

int main() {
    FILE *fp;
    char c[1];
    int number[1000];
    int i = 0;
    fp = fopen("1000.txt", "rt");
    while(!feof(fp) && i < 1000) {
        c[0] = fgetc(fp);
        int num = c[0] - '0';
        number[i++] = num;
    }
    close(fp);
    int window_end = 4;
    int max = 0;
    while(window_end < 1000) {
        if(number[window_end] !=0) {
            int temp = number[window_end - 4] * number[window_end - 3] * number[window_end - 2] * number[window_end - 1] * number[window_end];
            if(temp > max)
                max = temp;
            window_end++;
        } else {
            window_end = window_end + 5;
        }
    }
    printf("Largest product: %d\n",max);
    return 0;
}

