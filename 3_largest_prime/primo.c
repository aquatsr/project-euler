#include <stdio.h>

#define BOUND 600851475143

long thisBetterWork(long number) {
    long divisor = 2;
    while(number % divisor)
        divisor++;
    long quotient = (number / divisor);
    if(quotient == 1)
        return 0;
    int i = 2;
    int is_prime = 0;
    while(quotient % i) {
        if(i == quotient - 1) {
            is_prime = 1;
            break;
        } else {
            i++;
        }
    }
    int is_prime_2 = 0;
    i = 2;
    while(divisor % i) {
        if(i == divisor - 1) {
           is_prime_2 = 1;
           break;
        } else {
           i++;
        }
    } 
    if(is_prime)
        printf("Prime: %ld\n",quotient);
    else
        printf("Composite: %ld\n",quotient);
    if(is_prime_2)
        printf("Prime: %ld\n",divisor);
    else
        printf("Composite: %ld\n",divisor);
    thisBetterWork(quotient);
}

int main() {
    thisBetterWork(BOUND);
    return 0;
}
