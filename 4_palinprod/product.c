#include <stdio.h>

int rev_num = 0;
int reverse(int number) {
    if(number > 0) {
        rev_num = (rev_num * 10) + (number % 10);
        reverse(number / 10);
    }
    return rev_num;
} // reverse

int main() {
    int f, s, fxs, max, f_save, s_save;
    max = 0;
    for(f = 100; f < 1000; f++) 
        for(s = 100; s < 1000; s++) {
            fxs = f * s;
            rev_num = 0;
            if((fxs) == reverse(fxs))
                if(fxs > max) {
                    max = fxs;
                    f_save = f;
                    s_save = s;
                }
        }
    printf("%d x %d: %d\n",f_save,s_save,max);
    return 0;
}
        
