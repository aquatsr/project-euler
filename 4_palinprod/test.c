#include <stdio.h>

int rev_num = 0;
int reverse(int number) {
    if(number > 0) {
        rev_num = (rev_num * 10) + (number % 10);
        reverse(number / 10);
    }
    return rev_num;
} // reverse

int main() {
    printf("Enter a number: ");
    int num;
    scanf("%d",&num);

    if(num == reverse(num))
        printf("%d is a palindrome.\n", num);
    else
        printf("%d is not a palindrome.\n", num);

    return 0;
}
