#include <stdio.h>

#define ITS_OVER_ONE_MILLION (1000000 - 1)

long evenInput(long even) {
    return ((even >> 1));
}

long oddInput(long odd) {
    return ((3 * odd) + 1);
}

int main() {
    long longest = 0;
    long chain_count = 1;
    long current_num;
    long num;
    long longest_num = 0;
    for(current_num = ITS_OVER_ONE_MILLION; current_num > 1; current_num--) {
        num = current_num;
        while(num != 1) {
            if(!(num % 2)) { // even 
                num = evenInput(num);
            }
            else {
                num = oddInput(num);
            }
            chain_count++;
        }
        if(++chain_count > longest) {
            longest = chain_count;
            longest_num = current_num;
        }
        chain_count = 0;
        //printf("%ld\n",current_num);
    }
    printf("Longest chain: %ld from number: %ld\n",longest,longest_num);
    return 0;
}
