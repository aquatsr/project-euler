#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_ROW 100
#define MAX_COL 50

int main() {
    FILE *fp;
    char c[1];
    char burn[1];
    int grid[MAX_ROW][MAX_COL];
    int result[MAX_COL];
    int remainder_array[MAX_COL + 2] = {0};
    int header[2] = {0};
    int row = 0;
    int col = 0;
    
    for(row = 0; row < MAX_ROW; row++) {
        for(col = 0; col < MAX_COL; col++) {
            grid[row][col] = 0;
        }
    }

    row = 0;
    col = 0;
    
    fp = fopen("numbers.txt", "rt");
    while(!feof(fp)) {
        c[0] = fgetc(fp);
        //printf("%d",c[0]  - '0');
        grid[row][col] = c[0] - '0';
        col++;
        if(col >= MAX_COL) {
            col = 0;
            row++;
            burn[0] = fgetc(fp);
            burn[0] = fgetc(fp);
        }
    }
    close(fp);
    /*
    for(row = 0; row < MAX_ROW; row++) {
        for(col = 0; col < MAX_COL; col++) {
            printf("%d",grid[row][col]);
        }
        printf("\n");
    }
    */


    int colRTL = 0;
    int col_sum = 0;
    int i;
    for(col = MAX_COL - 1; col > -1; col--) {
        col_sum = remainder_array[colRTL];
        for(row = 0; row < MAX_ROW; row++) {
            col_sum += grid[row][col];
        }
        if(col != 0) {
            result[col] = col_sum % 10;
            remainder_array[++colRTL] += (col_sum / 10) % 10;
            remainder_array[colRTL + 1] += col_sum / 100;
            for(i = 0; i < MAX_COL; i++) {
                if(remainder_array[i] > 10) {
                    remainder_array[i + 1] += remainder_array[i] / 10;
                    remainder_array[i] = remainder_array[i] % 10;
                }
            }
        } else {
            printf("colRTL: %d\n",colRTL);
            result[col] = col_sum % 10;
            header[1] = ((col_sum / 10) % 10) + remainder_array[++colRTL];
            header[0] = (col_sum / 100);
        }
    }
    printf("%d%d",header[0],header[1]);
    for(col = 0; col < 8; col++)
        printf("%d",result[col]);
    printf("\n\nFull Result: ");
    printf("%d%d",header[0],header[1]);
    for(col = 0; col < MAX_COL; col++)
        printf("%d",result[col]);
    printf("\n");
    
    return 0;
}

