#include <stdio.h>

#define LIMIT 1000000

int main() {
    int pA[LIMIT] = {0};
    int testNum = 2;
    int primesFound = 1;
    pA[0] = 2;
    while(testNum < LIMIT) {
        testNum++;
        int i, flag = 1;
        for(i = 0; i < primesFound; i++) {
            if(!(testNum % pA[i])) {
                flag = 0;
                break;
            }
        }
        if(flag == 1)
            pA[primesFound++] = testNum;
    }
    int i, j, k; 
    long largest = 2;
    long sum = 0;
    for(i = 0; i < primesFound; i++) {
       k = 0;
       for(k; k < primesFound; k++) {
           sum = 0;
           for(j = k; j < primesFound; j++) {
               sum += pA[j];
               if(sum == pA[i] && sum > largest)
                   largest = pA[i];
           }
        }
    }

    printf("\nLargest: %ld\n",largest);

    return 0;
}
