#include <stdio.h>
#define SSTART 100

int count = 0;

int split(int start) {
    count++;
    if(start == 1)
        return 0;
    return(split(start - 1));
}

int main() {
    int start;
    for(start = SSTART; start > 0; start--)
        split(start);
    printf("%d ways\n",count-SSTART);
    return 0;
}
