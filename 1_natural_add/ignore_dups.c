#include <stdio.h>
#define THREE 3
#define PHIVE 5
#define LIMIT 105000000

int main() {
    long long threes = 9;
    long long phives = 15;
    long long i;
    for(i = 3; i < (LIMIT / THREE); i++) {
        if (((THREE * i) % 5)) 
            threes += THREE * i;
        if (i < (LIMIT / PHIVE))
            phives += PHIVE * i;
    }
    printf("%llu\n",(threes + phives));
    return 0;
}
