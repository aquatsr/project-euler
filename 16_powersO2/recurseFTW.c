#include <stdio.h>
#define EXPONENT 50

long sum = 0;

long recurse(long start) {
    if(start > 0)
        return(recurse(start - 1) + recurse(start - 1));
    else
        return (1);
}

long chopDigitsAndSum(long decimal) {
    long sum = 0;
    while(decimal > 0) {
        sum += decimal % 10;
        decimal = decimal / 10;
    }
    return sum;
}

int main() {
    long num = recurse(EXPONENT);
    long sum = chopDigitsAndSum(num);
    printf("%ld\n",sum);
    return 0;
}
