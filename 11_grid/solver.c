#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main() {
    FILE *fp;
    char c[1];
    char temp[1];
    char burn[1];
    int grid[20][20];
    int row = 0;
    int col = 0;
    
    for(row = 0; row < 20; row++) {
        for(col = 0; col < 20; col++) {
            grid[row][col] = 0;
        }
    }

    row = 0;
    col = 0;
    int count = 0;
    int overallCount = 0;
    
    fp = fopen("grid.txt", "rt");
    while(!feof(fp)) {
        if(count == 1) {
            c[0] = fgetc(fp);
            grid[row][col] = (10 * (temp[0] - '0')) + (1* (c[0] - '0'));
            col++;
            //printf("row: %d, col: %d\n",row, col);
            count++;
        } else if(count == 2) {
            burn[0] = fgetc(fp);
            count = 0;
            if(col > 19) {
                row++;
                col = 0;
            }
        } else {
            temp[0] = fgetc(fp);
            count++;
        }
    }
    close(fp);
    
    long horizontal_prod = 0;
    long vertical_prod = 0;
    long ltrdd = 0;
    long rtldd = 0;
    long max = 0;

    for(row = 0; row < 20; row++) {
        for(col = 0; col < 20; col++) {
            if(col + 3 < 20) {
                horizontal_prod = grid[row][col] * grid[row][col + 1] * grid[row][col + 2] * grid[row][col + 3];
                if(row + 3 < 20)
                    ltrdd = grid[row][col] * grid[row + 1][col + 1] * grid[row + 2][col + 2] * grid[row + 3][col + 3];
            }
            if(row + 3 < 20) {
                vertical_prod = grid[row][col] * grid[row + 1][col] * grid[row + 2][col] * grid[row + 3][col];
                if(col - 3 > 0)
                    rtldd = grid[row][col] * grid[row + 1][col - 1] * grid[row + 2][col - 2] * grid[row + 3][col - 3];
            }
            if(horizontal_prod > max)
                max = horizontal_prod;
            if(vertical_prod > max)
                max = vertical_prod;
            if(ltrdd > max)
                max = ltrdd;
            if(rtldd > max)
                max = rtldd;
        }
    }
    printf("Max product: %ld\n",max);
    return 0;
}

