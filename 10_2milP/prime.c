#include <stdio.h>

#define BOUND 1000

int main() {
    int iAmAPrime = 5;
    int iAmASum = 10;
    while(1) {
        iAmAPrime++;
        int iAmAGreen = 2; // green badge = temporary/contract worker
        while(iAmAPrime % iAmAGreen)
            if(iAmAGreen <= BOUND)
                iAmAGreen++;
            else
                break;
        if(iAmAGreen == BOUND)
           break;
        if(iAmAPrime == iAmAGreen)
            if(iAmAPrime <= BOUND)
                iAmASum += iAmAPrime;
            else
                break;
    }
    printf("Sum of primes < 100 %d\n",iAmASum);
    return 0;
}
    
