#include <stdio.h>

int prime_array[2000000] = {0};

long thisBetterWork(long number) {
    long divisor = 2;
    while(number % divisor)
        divisor++;
    long quotient = (number / divisor);
    if(quotient == 1) {
        prime_array[number] = 1;
        return 0;
    }
    int i = 2;
    int is_prime = 0;
    while(quotient % i) {
        if(i == quotient - 1) {
            is_prime = 1;
            break;
        } else {
            i++;
        }
    }
    int is_prime_2 = 0;
    i = 2;
    while(divisor % i) {
        if(i == divisor - 1) {
           is_prime_2 = 1;
           break;
        } else {
           i++;
        }
    }
    if(is_prime) {
        if(prime_array[quotient] == 1)
            return 0;
        else
            prime_array[quotient] = 1;
    }
    if(is_prime_2)
        prime_array[divisor] = 1;
    thisBetterWork(quotient);
}

int main() {
    int i;
    for(i = 2000000; i > 1; i--)
        thisBetterWork(i);
    int sum = 0;
    for(i = 0; i < 2000000; i++)
        if(prime_array[i] == 1)
            sum += i;
    printf("Sum: %d\n",sum);
    return 0;
}
