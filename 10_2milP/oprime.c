#include <stdio.h>

#define LIMIT 2000000

int main() {
    int pA[LIMIT] = {0};
    int testNum = 2;
    int primesFound = 1;
    pA[0] = 2;
    while(testNum < LIMIT) {
        testNum++;
        int i, flag = 1;
        for(i = 0; i < primesFound; i++) {
            if(!(testNum % pA[i])) {
                flag = 0;
                break;
            }
        }
        if(flag == 1)
            pA[primesFound++] = testNum;
    }
    int i; 
    long sum = 0;
    for(i = 0; i < primesFound; i++) {
        printf("%d, ",pA[i]);
        sum += pA[i];
    }

    printf("\nSum of %d primes: %ld\n",primesFound,sum);

    return 0;
}
