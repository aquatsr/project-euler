#include <stdio.h>

int main() {
    int iAmAPrime = 5;
    int iAmACount = 3;
    while(iAmACount < 10001) {
        iAmAPrime++;
        int iAmAGreen = 2; // green badge = temporary/contract worker
        while(iAmAPrime % iAmAGreen)
            iAmAGreen++;
        if(iAmAPrime == iAmAGreen)
            iAmACount++;
    }
    printf("%d prime: %d\n", iAmACount, iAmAPrime);
    return 0;
}
    
