#include <stdio.h>

#define GOAL 1000
#define BOUND 1000

int main() {
    int a, b, c;
    for(a = 0; a < BOUND; a++)
        for(b = 0; b < BOUND; b++)
            for(c = 0; c < BOUND; c++)
                if(a < b)
                    if(b < c)
                        if(((a * a) + (b * b) == (c * c)) && ((a + b + c) == GOAL))
                            printf("%d x %d x %d = %d\n",a,b,c,(a * b * c));
    return 0;
}
