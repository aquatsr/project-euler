#include <stdio.h>
#define PA_SIZE 10000
#define MIN_DIVISORS 500

int main() {
    int primesArray[PA_SIZE];
    int factorization[PA_SIZE] = {0};
    int startNum = 2;
    int primesFound = 1;
    primesArray[0] = 2;
    int i, flag = 1;
    while(primesFound < PA_SIZE) {
        startNum++;
        flag = 1;
        for(i = 0; i < primesFound; i++) {
            if(!(startNum % primesArray[i])) {
                flag = 0;
                break;
            }
        }
        if(flag == 1)
            primesArray[primesFound++] = startNum;
    }

    for(i = 2; i < 100000; i++) {
        int primesTested = 0;
        int triangleNum = (i * (i + 1)) / 2;
        while(triangleNum > 1) {
            if(!(triangleNum % primesArray[primesTested])) {
                factorization[primesTested] += 1;
                triangleNum = triangleNum / primesArray[primesTested];
            }
            else
                primesTested++;
        }

        int j;
        int divisors = 1;
        for(j = 0; j < primesTested + 1; j++) {
            if(factorization[j] != 0) {
                divisors = divisors * (factorization[j] + 1);
            }
            if(divisors > MIN_DIVISORS) {
                printf("Triangle Number w/ over %d divisors: %d\n",MIN_DIVISORS,((i * (i + 1))/2));
                return 0;
            }
            factorization[j] = 0;
        }
    }

    return 0;
}
